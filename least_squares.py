import numpy as np
from sklearn.preprocessing import OneHotEncoder

class LeastSquares():
	def __init__(self, l=0.0):
		self.l = l
		self.X = []
		self.Y = []
		self.W = []
		
	def __get_homogeneous__(self, x, add_column=False):
		if add_column:
			x_h = np.concatenate([np.ones([x.shape[0],1]),x], axis=1)
		else:
			x_h = np.concatenate([x,np.ones([1,x.shape[1]])], axis=0)
		return x_h

	def __get_pseudoinverse__(self,A):
		return np.dot(np.linalg.inv(np.dot(A.T, A) + self.l * np.identity(A.shape[1])), A.T)

	def fit(self,x, y):
		ohe = OneHotEncoder(sparse=False, categorical_features='all')
		self.Y = ohe.fit_transform(np.asarray([y]).T)
		self.X = self.__get_homogeneous__(x, add_column=True)
		self.W = np.dot(self.__get_pseudoinverse__(self.X), self.Y)

	def predict(self,x_test):
		X_test = self.__get_homogeneous__(x_test, add_column=True)
		Y_pred = np.dot(X_test, self.W)
		return np.argmax(Y_pred, axis=1)

if __name__ == '__main__':
	clf = LeastSquares()

	x = np.array([[0,0,0],[1,1,1],[2,2,2]])
	y = np.array([2,3,1])
	clf.fit(x,y)