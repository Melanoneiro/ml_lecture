import numpy as np
from joblib import load
from sklearn import metrics
from tensorflow.keras.models import load_model

from naive_bayes import GaussianNaiveBayes
from least_squares import LeastSquares
from one_stream_nn import NeuralNetwork1Stream
from two_stream_nn import NeuralNetwork2Stream
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

from plot_confusion_matrix import plot_confusion_matrix

def loadClassifier(clf_type):
	if clf_type == "network1":
		clf = NeuralNetwork1Stream("./classifiers/clf_network1.h5")

	elif clf_type == "network2":
		clf = NeuralNetwork2Stream("./classifiers/clf_network2.h5")

	else:
		clf = load("./classifiers/clf_" + clf_type + ".joblib")

	return clf

if __name__ == "__main__":

	print("Loading data...")

	source_dir = "/home/niko/Downloads/yt8m/data/yt8m/"
	classes = [15, 21, 27]
	clf_type = "network2" # gnb | knn | lsq | network1 | network2 | svm

	x = np.empty([0, 1024 + 128])
	y = np.array([])
	for c in classes:
		print("Class:", c)
		filename_video = source_dir + "class_" + "{:04d}".format(c) + "/validate_features_video.npy"
		filename_audio = source_dir + "class_" + "{:04d}".format(c) + "/validate_features_audio.npy"
		features_video = np.load(filename_video)
		features_audio = np.load(filename_audio)

		features = np.concatenate((features_video, features_audio), axis = 1)
		
		x = np.concatenate((x, features[0:10000]))
		y = np.append(y, np.ones(10000) * c)
		
	# convert original class ids into 0, 1, 2
	y = np.digitize(y.ravel(), classes, right=True)

	print("Getting classifier...")
	clf = loadClassifier(clf_type)

	print("Calculating predictions...")
	y_pred = clf.predict(x)

	confusion = metrics.confusion_matrix(y, y_pred)
	print("Confusion:")
	print(confusion)

	accuracy = metrics.accuracy_score(y, y_pred)
	print("Accuracy: ", accuracy)
	precision = metrics.precision_score(y, y_pred, average=None)
	print("Precision: ", precision)
	recall = metrics.recall_score(y, y_pred, average=None)
	print("Recall: ", recall)

	plot_confusion_matrix(y, y_pred, np.array([str(c) for c in classes]), normalize=True)