import google
import tensorflow
import json
import os
import numpy as np

# function read_file reads sequentially all records of a YouTube 8M shard (<name>.tfrecord)
# You may need to install certain packages depending on your distribution
# Modify the code to select 3 classes

def extractClassesFromShards(filename, label):
	ids            = np.empty((0, 1))
	features_video = np.empty((0, 1024))
	features_audio = np.empty((0, 128))

	for record in tensorflow.compat.v1.python_io.tf_record_iterator(filename):
		record = tensorflow.train.Example.FromString(record)
		json_msg = google.protobuf.json_format.MessageToJson(record)
		json_msg = json.loads(json_msg)

		id     = record.features.feature["id"].bytes_list.value[0].decode("utf-8")
		labels = json_msg["features"]["feature"]["labels"]["int64List"]["value"]
		labels = [int(l) for l in labels]

		if label in labels:
			ids = np.append(ids, id)
			features_video = np.append(features_video, [json_msg["features"]["feature"]["mean_rgb"]["floatList"]["value"]], axis = 0)
			features_audio = np.append(features_audio, [json_msg["features"]["feature"]["mean_audio"]["floatList"]["value"]], axis = 0)

	return ids, features_video, features_audio


if __name__ == "__main__":
	source_dir = "/home/niko/Downloads/yt8m/data/yt8m/video/"
	num_shards = 3844
	label = int(input("Number of label to be extracted: "))
	sample_type = "validate"

	try:
		os.mkdir(source_dir + "../class_" + "{:04d}".format(label))
	except Exception as e:
		print(e)

	ids            = np.empty((0, 1))
	features_video = np.empty((0, 1024))
	features_audio = np.empty((0, 128))
	for i in range(num_shards):
		print("Reading shard #" + "{:04d}".format(i))
		filename_shard = source_dir + sample_type + "{:04d}".format(i) + ".tfrecord"
		(i, v, a) = extractClassesFromShards(filename_shard, label)
		ids            = np.append(ids, i)
		features_video = np.append(features_video, v, axis = 0)
		features_audio = np.append(features_audio, a, axis = 0)

	filename_ids   = source_dir + "../class_" + "{:04d}".format(label) + "/" + sample_type + "_ids.npy"
	filename_video = source_dir + "../class_" + "{:04d}".format(label) + "/" + sample_type + "_features_video.npy"
	filename_audio = source_dir + "../class_" + "{:04d}".format(label) + "/" + sample_type + "_features_audio.npy"
	np.save(filename_ids, ids)
	np.save(filename_video, features_video)
	np.save(filename_audio, features_audio)

	print("Done. Extracted ", ids.size, " records.")