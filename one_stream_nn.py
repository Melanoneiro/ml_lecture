from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential, Model, load_model
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.optimizers import Adam

import os
import numpy as np

class NeuralNetwork1Stream():
	def __init__(self, from_file = None):
		if from_file != None:
			self.model = load_model(from_file)

		else: 
			self.model = Sequential()
			self.model.add(Dense(200, input_shape=(1024 + 128,), activation='relu'))
			#self.model.add(Dense(300, activation='sigmoid'))
			self.model.add(Dropout(0.2))
			self.model.add(Dense(  3, activation='softmax'))

			opt = Adam(learning_rate=0.001)
			self.model.compile(loss='categorical_crossentropy', optimizer=opt)

			try:
				os.mkdir("./classifiers")
			except:
				pass
			save_model_name = os.path.join('./classifiers/clf_network1.h5')
			self.saveBest = ModelCheckpoint(save_model_name, monitor='val_loss', save_best_only=True)
			self.earlyStopping = EarlyStopping(monitor='val_loss', min_delta=0.0001, patience=3, verbose=2, mode='auto')

	def fit(self, x, y):
		y_categorical = to_categorical(y, 3)
		self.model.fit(x, y_categorical, batch_size=32, epochs=10, verbose=1, validation_split=0.05, callbacks=[self.saveBest, self.earlyStopping])

	def predict(self, x_test):
		y_pred = self.model.predict(x_test)
		return np.argmax(y_pred, axis=1)
