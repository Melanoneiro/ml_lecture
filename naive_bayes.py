from scipy.stats import norm as gauss
import numpy as np

class GaussianNaiveBayes():
        def __init__(self):
                self.prior = []
                self.mean = []
                self.std = []
                self.gauss = []
                self.num_labels = 0

        def __get_posterior__(self,x_test,target_class):
                return np.log(self.prior[target_class]) + np.sum(self.gauss[target_class].logpdf(x_test))

        def fit(self,x,y):
                self.num_labels = len(list(np.unique(y)))
                for cc in range(self.num_labels):
                        x_partial = x[y==cc]
                        self.prior.append( 1.0*x_partial.shape[0] / x.shape[0] )
                        self.mean.append( np.mean(x_partial, axis=0) )
                        self.std.append( np.std(x_partial, axis=0) )
                        self.gauss.append( gauss(self.mean[-1], self.std[-1]) )

        def predict(self,x_test):
                pred = []
                for ss in range(x_test.shape[0]):
                        sample_pred = []
                        for cc in range(self.num_labels):
                                sample_pred.append( self.__get_posterior__(x_test[ss],cc) )
                        pred.append( np.argmax(sample_pred) )
                return np.asarray(pred)
