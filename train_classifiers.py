import numpy as np
from joblib import dump
import time
import os

from naive_bayes import GaussianNaiveBayes
from least_squares import LeastSquares
from one_stream_nn import NeuralNetwork1Stream
from two_stream_nn import NeuralNetwork2Stream
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn import metrics

def getClassifier(clf_type):
	if clf_type == "gnb":
		clf = GaussianNaiveBayes()

	if clf_type == "knn":
		clf = KNeighborsClassifier(n_neighbors = 5, n_jobs = 8)

	if clf_type == "lsq":
		clf = LeastSquares()

	if clf_type == "network1":
		clf = NeuralNetwork1Stream()

	if clf_type == "network2":
		clf = NeuralNetwork2Stream()

	if clf_type == "svm":
		clf = SVC(kernel = "rbf", C=10.0)

	return clf

if __name__ == "__main__":

	print("Loading data...")

	source_dir = "/home/niko/Downloads/yt8m/data/yt8m/"
	classes = [15, 21, 27]
	clf_type = "network2" # gnb | knn | lsq | network1 | network2 | svm

	# Import the data
	x = np.empty([0, 1024 + 128])
	y = np.array([])
	for c in classes:
		print("Class:", c)
		filename_video = source_dir + "class_" + "{:04d}".format(c) + "/train_features_video.npy"
		filename_audio = source_dir + "class_" + "{:04d}".format(c) + "/train_features_audio.npy"
		features_video = np.load(filename_video)
		features_audio = np.load(filename_audio)

		features = np.concatenate((features_video, features_audio), axis = 1)
		
		x = np.concatenate((x, features[0:10000]))
		y = np.append(y, np.ones(10000) * c)
		
	# convert original class ids into 0, 1, 2
	y = np.digitize(y.ravel(), classes, right=True)
	
	print("Done.")


	# Perform 5-fold validation and store all classifers
	classifiers = np.array([])
	scores = np.array([])

	print("Creating models")
	cv = KFold(n_splits = 5)
	for i, (train_idx, test_idx) in enumerate(cv.split(x)):
		print("Running 5-fold validation, iteration", i + 1)

		t_start = time.time()

		# Split data for 5-fold validation
		x_train, x_test = x[train_idx], x[test_idx]
		y_train, y_test = y[train_idx], y[test_idx]

		# Classify data and measure performance
		clf = getClassifier(clf_type)
		clf.fit(x_train, y_train)
		y_pred = clf.predict(x_test)
		score = metrics.accuracy_score(y_test, y_pred)

		# Store classifier
		classifiers = np.append(classifiers, clf)
		scores = np.append(scores, score)

		t_end = time.time()
		print("Done in", int(t_end - t_start), " seconds. Accuracy:", score)

	# Use classifier with median performance
	median_idx = np.where(scores == np.median(scores))[0][0]
	print("Choosing classifier #", median_idx + 1)

	# Store model
	try:
		os.mkdir("./classifiers")
	except:
		pass

	if clf_type != "network1" and clf_type != "network2":
		dump(classifiers[median_idx], "./classifiers/clf_" + clf_type + ".joblib") 