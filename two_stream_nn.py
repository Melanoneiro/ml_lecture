from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential, Model, load_model
from tensorflow.keras.layers import Dense, Concatenate, Dropout, Input
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.optimizers import Adam

import os
import numpy as np

class NeuralNetwork2Stream():
	def __init__(self, from_file = None):
		if from_file != None:
			self.model = load_model(from_file)

		else: 
			video_input = Input((1024,))
			audio_input = Input((128,))
			
			model_v = Sequential()
			model_v.add(Dense(100, input_shape=(1024,), activation='relu'))
			model_v.add(Dropout(0.2))
			
			model_a = Sequential()
			model_a.add(Dense(100, input_shape=(128,), activation='relu'))
			model_a.add(Dropout(0.2))
			
			concat = Concatenate(axis=1)([model_v(video_input), model_a(audio_input)])
			
			out = Dense(3, activation='softmax')(concat)
			
			self.model = Model(inputs=[video_input, audio_input], outputs=out)
			opt = Adam(learning_rate=0.001)
			self.model.compile(loss='categorical_crossentropy', optimizer=opt)
			
			try:
				os.mkdir("./classifiers")
			except:
				pass
			save_model_name = os.path.join('./classifiers/clf_network2.h5')
			self.saveBest = ModelCheckpoint(save_model_name, monitor='val_loss', save_best_only=True)
			self.earlyStopping = EarlyStopping(monitor='val_loss', min_delta=0.0001, patience=3, verbose=2, mode='auto')
		
	def fit(self, x, y):
		y_categorical = to_categorical(y, 3)
		self.model.fit([x[:,:1024], x[:,1024:]], y_categorical, batch_size=64, epochs=10, verbose=1, validation_split=0.05, callbacks=[self.saveBest, self.earlyStopping])
		
	def predict(self, x_test):
		y_pred = self.model.predict([x_test[:,:1024], x_test[:,1024:]])
		return np.argmax(y_pred, axis=1)
